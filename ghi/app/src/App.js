import {BrowserRouter, Route, Routes} from "react-router-dom";
import Nav from './Nav';
import AttendeesList from './AttendeeList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import MainPage from "./MainPage";


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="attendees">
            <Route index element={<AttendeesList />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
